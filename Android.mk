LOCAL_PATH:= $(call my-dir)

ifneq ($(filter surya,$(TARGET_DEVICE)),)

# Firmware
FIRMWARE_IMAGES := $(wildcard $(LOCAL_PATH)/images/*)

$(foreach f, $(notdir $(FIRMWARE_IMAGES)), \
    $(call add-radio-file,images/$(f)))

$(call add-radio-file,filesmap)

endif

